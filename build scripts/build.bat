PUSHD ..
pico8 "../racer8.p8" -export "-w race.js"
pico8 "../racer8.p8" -export "acceler8.bin"
POPD

7z a ..\builds\acceler8.zip @web-build.txt
IF ERRORLEVEL 1 GOTO :error
7z a ..\builds\acceler8-win.zip @win-build.txt
IF ERRORLEVEL 1 GOTO :error
7z a ..\builds\acceler8-mac.zip @mac-build.txt
IF ERRORLEVEL 1 GOTO :error
7z a ..\builds\acceler8-linux.zip @linux-build.txt
IF ERRORLEVEL 1 GOTO :error
7z a ..\builds\acceler8-raspi.zip @raspi-build.txt
IF ERRORLEVEL 1 GOTO :error
GOTO :done

:error
EXIT /B 1

:done
