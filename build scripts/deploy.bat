butler push ..\builds\acceler8.zip --if-changed BrightMothGames/Acceler-8:html
IF ERRORLEVEL 1 GOTO :error 
butler push ..\builds\acceler8-linux.zip --if-changed BrightMothGames/Acceler-8:linux
IF ERRORLEVEL 1 GOTO :error
butler push ..\builds\acceler8-win.zip --if-changed BrightMothGames/Acceler-8:win
IF ERRORLEVEL 1 GOTO :error
butler push ..\builds\acceler8-mac.zip --if-changed BrightMothGames/Acceler-8:mac
IF ERRORLEVEL 1 GOTO :error
butler push ..\builds\acceler8-raspi.zip --if-changed BrightMothGames/Acceler-8:raspi
IF ERRORLEVEL 1 GOTO :error
GOTO :done

:error
EXIT /B 1

:done
PAUSE